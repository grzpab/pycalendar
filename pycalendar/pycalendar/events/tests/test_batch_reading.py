# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import datetime

from dateutil.relativedelta import relativedelta
from rest_framework.test import APITestCase

from pycalendar.events.models import EventType
from pycalendar.tests import Calendar
from pycalendar.tests import CalendarAdapter
from pycalendar.tests import GlobalEvent
from pycalendar.tests import GlobalEventAdapter
from pycalendar.tests import LocalEventAdapter
from pycalendar.tests import User
from pycalendar.tests import UserManager


class TestBatchReading(APITestCase):
    def setUp(self):
        super(TestBatchReading, self).setUp()

        self.user1 = User.create()
        self.user2 = User.create()

        self.user1_manager = UserManager(client=self.client, user=self.user1)
        self.user2_manager = UserManager(client=self.client, user=self.user2)

        self.user1_manager.create()
        self.user2_manager.create()

    def test_event_time_endpoints(self):
        today = datetime.date.today()

        with self.user1_manager.do_as_user():
            calendar = Calendar(name='My first calendar')
            calendar_adapter = CalendarAdapter(
                client=self.client,
                calendar=calendar,
            )

            calendar_adapter.create()

            global_event = GlobalEvent(
                title='Title',
                description='Description',
                event_type=EventType.ALL_DAY,
                starting_date_time=None,
                ending_date_time=None,
                day=today,
                calendar_pk=calendar.pk,
            )

            global_event_adapter = GlobalEventAdapter(
                client=self.client,
                global_event=global_event,
            )

            global_event_adapter.create(users=[self.user2.pk])

            response_data = GlobalEventAdapter.read_all_by_date(
                client=self.client,
                date_=today,
                zone='-7',
            )
            self.assertEqual(len(response_data), 1)

            response_data = GlobalEventAdapter.read_all_by_month(
                client=self.client,
                date_=today,
                zone='-7',
            )
            self.assertEqual(len(response_data), 1)

            response_data = GlobalEventAdapter.read_all_by_year(
                client=self.client,
                date_=today,
                zone='-7',
            )
            self.assertEqual(len(response_data), 1)

            # out of reach

            response_data = GlobalEventAdapter.read_all_by_date(
                client=self.client,
                date_=today + relativedelta(days=1),
                zone = '-7',
            )
            self.assertEqual(len(response_data), 0)

            response_data = GlobalEventAdapter.read_all_by_month(
                client=self.client,
                date_=today + relativedelta(months=1),
                zone='-7',
            )
            self.assertEqual(len(response_data), 0)

            response_data = GlobalEventAdapter.read_all_by_year(
                client=self.client,
                date_=today + relativedelta(years=1),
                zone='-7',
            )
            self.assertEqual(len(response_data), 0)

        def get_the_local_events():
            response_data = LocalEventAdapter.read_all_by_date(
                client=self.client,
                date_=today,
                zone='-7',
            )
            self.assertEqual(len(response_data), 1)

            response_data = LocalEventAdapter.read_all_by_month(
                client=self.client,
                date_=today,
                zone='-7',
            )
            self.assertEqual(len(response_data), 1)

            response_data = LocalEventAdapter.read_all_by_year(
                client=self.client,
                date_=today,
                zone='-7',
            )
            self.assertEqual(len(response_data), 1)

        with self.user1_manager.do_as_user():
            get_the_local_events()

        with self.user2_manager.do_as_user():
            get_the_local_events()

    def test_event_period_time_endpoints(self):
        with self.user1_manager.do_as_user():
            calendar = Calendar(name='My first calendar')
            calendar_adapter = CalendarAdapter(
                client=self.client,
                calendar=calendar,
            )

            calendar_adapter.create()

            global_event = GlobalEvent(
                title='Title',
                description='Description',
                event_type=EventType.PERIOD,
                starting_date_time='2016-08-08T11:12:51.679911Z',
                ending_date_time='2016-08-08T14:12:51.679911Z',
                day=None,
                calendar_pk=calendar.pk,
            )

            global_event_adapter = GlobalEventAdapter(
                client=self.client,
                global_event=global_event,
            )

            global_event_adapter.create(users=[])

            response_data = GlobalEventAdapter.read_all_by_date(
                client=self.client,
                date_=datetime.datetime(year=2016, month=8, day=8),
                zone='0',
            )
            self.assertEqual(len(response_data), 1)

            response_data = GlobalEventAdapter.read_all_by_date(
                client=self.client,
                date_=datetime.datetime(year=2016, month=8, day=8),
                zone='-11',
            )
            self.assertEqual(len(response_data), 1)

            response_data = GlobalEventAdapter.read_all_by_date(
                client=self.client,
                date_=datetime.datetime(year=2016, month=8, day=9),
                zone='11',
            )
            self.assertEqual(len(response_data), 1)
