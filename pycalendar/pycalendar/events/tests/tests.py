# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import datetime

from rest_framework.test import APITestCase

from pycalendar.calendars.models import CalendarAccessLevel
from pycalendar.events.models import EventType

from pycalendar.tests import Calendar
from pycalendar.tests import CalendarAccess
from pycalendar.tests import CalendarAccessAdapter
from pycalendar.tests import CalendarAdapter
from pycalendar.tests import User
from pycalendar.tests import UserManager

from pycalendar.tests import GlobalEvent
from pycalendar.tests import GlobalEventAdapter


class TestEvents(APITestCase):
    def setUp(self):
        super(TestEvents, self).setUp()

        self.user1 = User.create()
        self.user2 = User.create()
        self.user3 = User.create()

        self.user1_manager = UserManager(client=self.client, user=self.user1)
        self.user2_manager = UserManager(client=self.client, user=self.user2)
        self.user3_manager = UserManager(client=self.client, user=self.user3)

        self.user1_manager.create()
        self.user2_manager.create()
        self.user3_manager.create()

    def test_crud_operations_with_local_events(self):
        today = datetime.date.today()

        with self.user1_manager.do_as_user():
            calendar = Calendar(name='My first calendar')
            calendar_adapter = CalendarAdapter(
                client=self.client,
                calendar=calendar,
            )

            calendar_adapter.create()

            global_event = GlobalEvent(
                title='Title',
                description='Description',
                event_type=EventType.ALL_DAY,
                starting_date_time=None,
                ending_date_time=None,
                day=today,
                calendar_pk=calendar.pk,
            )

            global_event_adapter = GlobalEventAdapter(
                client=self.client,
                global_event=global_event,
            )

            response_data = self.cast_to_dict(
                global_event_adapter.create(users=[self.user2.pk, ])
            )

            self.assertDictEqual(dict(response_data), {
                'id': global_event.pk,
                'user': self.user1.pk,
                'calendar': calendar.pk,
                'title': 'Title',
                'description': 'Description',
                'event_type': 'ALL_DAY',
                'day': today.isoformat(),
                'starting_date_time': None,
                'ending_date_time': None,
                'local_events': [
                    {
                        'id': response_data['local_events'][0]['id'],
                        'user': self.user2.pk,
                        'rsvp': 'UNKNOWN',
                        'global_event': global_event.pk,
                    },
                ],
            })

            update_response_data = global_event_adapter.update(
                title='New title',
                description='New description',
                event_type=EventType.PERIOD,
                starting_date_time='2016-08-08 04:12:51.679911-07:00',
                ending_date_time='2016-08-09 04:12:51.679911-07:00',
            )

            self.assertDictEqual(self.cast_to_dict(update_response_data), {
                'event_type': 'PERIOD',
                'title': 'New title',
                'day': None,
                'user': self.user1.pk,
                'calendar': calendar.pk,
                'starting_date_time': '2016-08-08T04:12:51.679911-07:00',
                'ending_date_time': '2016-08-09T04:12:51.679911-07:00',
                'id': update_response_data['id'],
                'description': 'New description',
                'local_events': [
                    {
                        'id': response_data['local_events'][0]['id'],
                        'user': self.user2.pk,
                        'rsvp': 'UNKNOWN',
                        'global_event': global_event.pk,
                    },
                ],
            })

            reading_response_data = global_event_adapter.read()

            self.assertDictEqual(self.cast_to_dict(reading_response_data), {
                'event_type': 'PERIOD',
                'title': 'New title',
                'day': None,
                'user': self.user1.pk,
                'calendar': calendar.pk,
                'starting_date_time': '2016-08-08T11:12:51.679911Z',
                'ending_date_time': '2016-08-09T11:12:51.679911Z',
                'id': response_data['id'],
                'description': 'New description',
                'local_events': [
                    {
                        'id': response_data['local_events'][0]['id'],
                        'user': self.user2.pk,
                        'rsvp': 'UNKNOWN',
                        'global_event': global_event.pk,
                    },
                ],
            })

            global_event_adapter.delete()

    def test_crud_operations_with_calendar_access(self):
        today = datetime.date.today()

        with self.user1_manager.do_as_user():
            calendar = Calendar(name='My first calendar')
            calendar_adapter = CalendarAdapter(
                client=self.client,
                calendar=calendar,
            )

            calendar_adapter.create()

            global_event = GlobalEvent(
                title='Title',
                description='Description',
                event_type=EventType.ALL_DAY,
                starting_date_time=None,
                ending_date_time=None,
                day=today,
                calendar_pk=calendar.pk,
            )

            global_event_adapter = GlobalEventAdapter(
                client=self.client,
                global_event=global_event,
            )

            global_event_adapter.create(users=None)

        with self.user2_manager.do_as_user():
            response_data = GlobalEventAdapter.read_all(self.client)
            self.assertEqual(response_data, [])

            with self.assertRaises(Exception):
                global_event_adapter.read()

            # grant the privileges
            calendar_access = CalendarAccess(
                user=self.user2,
                calendar=calendar,
                access_level=CalendarAccessLevel.READ_EVENTS
            )

            calendar_access_adapter = CalendarAccessAdapter(
                client=self.client,
                calendar_access=calendar_access,
            )

            with self.assertRaises(Exception):
                # the interested user cannot create access entry for himself
                calendar_access_adapter.create()

        with self.user1_manager.do_as_user():
            calendar_access_adapter.create()

        with self.user2_manager.do_as_user():
            # C operation is not achievable
            # R operation is permitted
            # UD operations are illegal
            response_data = GlobalEventAdapter.read_all(self.client)
            self.assertEqual(len(response_data), 1)

            response_data = global_event_adapter.read()
            self.assertNotEqual(response_data, {})

            with self.assertRaises(Exception):
                global_event_adapter.delete()

            with self.assertRaises(Exception):
                global_event_adapter.update()

            with self.assertRaises(Exception):
                calendar_access_adapter.update(
                    access_level=CalendarAccessLevel.WRITE_ON_EVENTS
                )

            with self.assertRaises(Exception):
                calendar_access_adapter.delete()

        with self.user1_manager.do_as_user():
            calendar_access_adapter.update(
                access_level=CalendarAccessLevel.WRITE_ON_EVENTS
            )

        with self.user2_manager.do_as_user():
            # C operation is not achievable
            # RUD operations are permitted
            response_data = GlobalEventAdapter.read_all(self.client)
            self.assertEqual(len(response_data), 1)

            response_data = global_event_adapter.read()
            self.assertNotEqual(response_data, {})

            global_event_adapter.update()
            global_event_adapter.delete()

    @staticmethod
    def cast_to_dict(response_data):
        response_data = dict(response_data)

        response_data['local_events'] = [
            dict(le) for le in response_data['local_events']
        ]

        return response_data
