# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from .serializers import GlobalEventCreationSerializer
from .serializers import GlobalEventReadingSerializer
from .serializers import LocalEventCreationForGlobalOwnerSerializer
from .serializers import LocalEventForLocalOwnerSerializer
from .serializers import LocalEventForGlobalOwnerSerializer
from .models import GlobalEvent
from .models import LocalEvent

from .services import EventService
from .services import GlobalEventService
from .services import LocalEventService


class GlobalEventView(viewsets.ModelViewSet):
    queryset = GlobalEvent.objects.all()
    serializer_class = GlobalEventReadingSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        queryset = super(GlobalEventView, self).get_queryset()
        return EventService.get_event_queryset(queryset, self.kwargs)

    def create(self, request, *args, **kwargs):
        GlobalEventService.inject_user_from_request(
            user=request.user,
            data=request.data
        )

        serializer = GlobalEventCreationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)

        return Response(
            data=serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers
        )

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self._get_object_for_update_or_deletion()
        serializer = self.get_serializer(
            instance,
            data=request.data,
            partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self._get_object_for_update_or_deletion()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def filter_queryset(self, queryset):
        queryset = super(GlobalEventView, self).filter_queryset(queryset)

        return GlobalEventService.filter_queryset(
            user=self.request.user,
            queryset=queryset,
            is_readonly=True,
        )

    def _get_object_for_update_or_deletion(self):
        queryset = GlobalEventService.filter_queryset(
            user=self.request.user,
            queryset=self.get_queryset(),
            is_readonly=False,
        )

        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        assert lookup_url_kwarg in self.kwargs

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        self.check_object_permissions(self.request, obj)

        return obj


class LocalEventView(viewsets.ModelViewSet):
    queryset = LocalEvent.objects.all()
    serializer_class = LocalEventForGlobalOwnerSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        queryset = super(LocalEventView, self).get_queryset()
        return EventService.get_event_queryset(queryset, self.kwargs)

    def create(self, request, *args, **kwargs):
        user_id = request.user.id
        g_event_id = request.data['global_event']

        if not LocalEventService.has_access_for_creation(user_id, g_event_id):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        serializer = LocalEventCreationForGlobalOwnerSerializer(
            data=request.data
        )
        serializer.is_valid(raise_exception=True)

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED,
                        headers=headers)

    def retrieve(self, request, *args, **kwargs):
        user = request.user
        local_event = self.get_object()

        if not LocalEventService.has_access_for_reading(user, local_event):
            return Response(status=status.HTTP_404_NOT_FOUND)

        if local_event.user.id == user.id:
            serializer = LocalEventForLocalOwnerSerializer
        else:
            assert local_event.global_event.user.id == user.id
            serializer = LocalEventForGlobalOwnerSerializer

        serializer = serializer(local_event)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        user = request.user
        local_event = self.get_object()

        if not LocalEventService.has_access_for_update(user, local_event):
            return Response(status=status.HTTP_404_NOT_FOUND)

        partial = kwargs.pop('partial', False)

        serializer = LocalEventForLocalOwnerSerializer(
            local_event,
            data=request.data,
            partial=partial
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        user = request.user
        local_event = self.get_object()

        if not LocalEventService.has_access_for_deletion(user, local_event):
            return Response(status=status.HTTP_404_NOT_FOUND)

        self.perform_destroy(local_event)
        return Response(status=status.HTTP_204_NO_CONTENT)
