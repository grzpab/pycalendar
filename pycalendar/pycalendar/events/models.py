# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User
from django.db import models
from enumfields import IntEnum
from enumfields import EnumField


class EventType(IntEnum):
    ALL_DAY = 1
    PERIOD = 2

    class Labels(object):
        ALL_DAY = 'The event is going to take place the entire day'
        PERIOD = 'The event is going to take place within a stated period.'


class Rsvp(IntEnum):
    UNKNOWN = 1
    NO = 2
    MAYBE = 3
    YES = 4

    class Labels(object):
        UNKNOWN = 'The response to the invitation is unknown.'
        NO = 'The invitation has been rejected.'
        MAYBE = 'The invitee might participate in the event.'
        YES = 'The invitation has been accepted'


class Event(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    title = models.CharField(null=False, blank=False, max_length=255)
    description = models.CharField(null=True, blank=True, max_length=255)
    event_type = EnumField(EventType, null=False)
    starting_date_time = models.DateTimeField(null=True)
    ending_date_time = models.DateTimeField(null=True)
    day = models.DateField(null=True)

    class Meta(object):
        abstract = True


class GlobalEvent(Event):
    calendar = models.ForeignKey('calendars.Calendar')


class LocalEvent(Event):
    global_event = models.ForeignKey(GlobalEvent, null=False, related_name='local_events')
    rsvp = EnumField(Rsvp, null=False)
