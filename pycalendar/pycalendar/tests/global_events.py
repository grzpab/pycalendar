# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.urls import reverse
from rest_framework import status

from pycalendar.events.models import EventType
from pycalendar.tests.commons import RestApiAdapter


class GlobalEvent(object):
    def __init__(self, title, description, event_type, starting_date_time,
                 ending_date_time, day, calendar_pk, user_pk=None):
        """
        :type title: basestring
        :type description: basestring
        :type event_type: pycalendar.events.models.EventType
        :type starting_date_time: datetime.datetime | basestring | None
        :type ending_date_time: datetime.datetime | basestring | None
        :type day: datetime.date | None
        :type calendar_pk: pycalendar.tests.Calendar
        :type user_pk: pycalendar.tests.User | None
        """
        self.title = title
        self.description = description
        self.event_type = event_type
        self.starting_date_time = starting_date_time
        self.ending_date_time = ending_date_time
        self.day = day
        self.calendar_pk = calendar_pk
        self.user_pk = user_pk
        self.pk = None


class GlobalEventAdapter(RestApiAdapter):
    def __init__(self, client, global_event):
        """
        :type client: rest_framework.test.APIClient
        :type global_event: pycalendar.tests.GlobalEvent
        """
        self.client = client
        self.global_event = global_event

    def create(self, users):
        """
        :type users: list[int]

        :rtype: dict
        """
        data = self._get_create_or_update_payload(users=users)

        response = self.client.post(
            path=reverse('global-event-list'),
            data=data,
            format='json',
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.data)

        self.global_event.pk = response.data['id']
        self.global_event.user_pk = response.data['user']

        return response.data

    def update(self, title=None, description=None, event_type=None,
               starting_date_time=None, ending_date_time=None, day=None,
               calendar_pk=None):
        """
        :type title: basestring | None
        :type description: basestring | None
        :type event_type: pycalendar.events.models.EventType | None
        :type starting_date_time: datetime.datetime | basestring | None
        :type ending_date_time: datetime.datetime | basestring | None
        :type day: datetime.date | None
        :type calendar_pk: int | None

        :rtype: dict
        """

        data = self._get_create_or_update_payload(
            title=title,
            description=description,
            event_type=event_type,
            starting_date_time=starting_date_time,
            ending_date_time=ending_date_time,
            day=day,
            calendar_pk=calendar_pk,
        )

        response = self.client.put(
            path=reverse(
                'global-event-detail',
                kwargs={
                    'pk': self.global_event.pk,
                },
            ),
            data=data,
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        global_event = self.global_event

        global_event.title = response.data['title']
        global_event.description = response.data['description']
        global_event.event_type = response.data['event_type']
        global_event.starting_date_time = response.data['starting_date_time']
        global_event.ending_date_time = response.data['ending_date_time']
        global_event.day = response.data['day']
        global_event.calendar_pk = response.data['calendar']

        return response.data

    def read(self):
        """
        :rtype: dict
        """
        response = self.client.get(
            path=reverse(
                'global-event-detail',
                kwargs={
                    'pk': self.global_event.pk,
                },
            ),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    @staticmethod
    def read_all(client):
        """
        :type client: rest_framework.test.APIClient
        :rtype: dict
        """

        response = client.get(
            path=reverse('global-event-list'),
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    @staticmethod
    def read_all_by_year(client, date_, zone):
        """
        :type client: rest_framework.test.APIClient
        :type date_: datetime.date
        :type zone: basestring

        :rtype: dict
        """
        response = client.get(
            path=reverse(
                'global-event-list-by-y',
                kwargs={
                    'year': date_.strftime('%Y'),
                    'zone': zone,
                },
            ),
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    @staticmethod
    def read_all_by_month(client, date_, zone):
        """
        :type client: rest_framework.test.APIClient
        :type date_: datetime.date
        :type zone: basestring

        :rtype: dict
        """
        response = client.get(
            path=reverse(
                'global-event-list-by-ym',
                kwargs={
                    'year': date_.strftime('%Y'),
                    'month': date_.strftime('%m'),
                    'zone': zone,
                },
            ),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    @staticmethod
    def read_all_by_date(client, date_, zone):
        """
        :type client: rest_framework.test.APIClient
        :type date_: datetime.date
        :type zone: basestring

        :rtype: dict
        """
        response = client.get(
            path=reverse(
                'global-event-list-by-ymd',
                kwargs={
                    'year': date_.strftime('%Y'),
                    'month': date_.strftime('%m'),
                    'day': date_.strftime('%d'),
                    'zone': zone,
                },
            ),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    def delete(self):
        """
        :rtype: None
        """
        response = self.client.delete(
            path=reverse(
                'global-event-detail',
                kwargs={
                    'pk': self.global_event.pk,
                },
            ),
            format='json',
        )

        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise Exception(response.data)

        self.global_event = None

    def _get_create_or_update_payload(self, title=None, description=None,
                                      event_type=None, starting_date_time=None,
                                      ending_date_time=None, day=None,
                                      calendar_pk=None, users=None):
        """
        :type title: basestring | None
        :type description: basestring | None
        :type event_type: pycalendar.events.models.EventType | None
        :type starting_date_time: datetime.datetime | None
        :type ending_date_time: datetime.datetime | None
        :type day: datetime.date | None
        :type calendar_pk: int | None
        :type users: list[int] | None
        """
        if title is None:
            title = self.global_event.title

        if description is None:
            description = self.global_event.description

        if event_type is None:
            event_type = self.global_event.event_type

        if calendar_pk is None:
            calendar_pk = self.global_event.calendar_pk

        data = {
            'title': title,
            'description': description,
            'event_type': int(event_type),
            'calendar': calendar_pk
        }

        if event_type == EventType.ALL_DAY:
            day = self.global_event.day if day is None else day

            data['day'] = day
            data['starting_date_time'] = None
            data['ending_date_time'] = None
        elif event_type == EventType.PERIOD:
            if starting_date_time is None:
                starting_date_time = self.global_event.starting_date_time

            if ending_date_time is None:
                ending_date_time = self.global_event.ending_date_time

            data['day'] = None
            data['starting_date_time'] = starting_date_time
            data['ending_date_time'] =  ending_date_time
        else:
            raise Exception('Unsupported EventType')

        if self.global_event.pk is not None:
            data['id'] = self.global_event.pk

        if self.global_event.user_pk:
            data['user'] = self.global_event.user_pk,

        if users is not None:
            data['users'] = users

        return data
