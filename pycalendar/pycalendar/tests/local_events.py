# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import time

from django.urls import reverse
from rest_framework import status

from .commons import RestApiAdapter
from pycalendar.events.models import EventType


def local_time_offset(t=None):
    """Return offset of local zone from GMT, either at present or at time t."""
    # python2.3 localtime() can't take None
    if t is None:
        t = time.time()

    if time.localtime(t).tm_isdst and time.daylight:
        return -time.altzone
    else:
        return -time.timezone


class LocalEvent(object):
    def __init__(self, user, global_event, title=None, description=None,
                 event_type=None, starting_date_time=None,
                 ending_date_time=None, day=None, rsvp=None):
        """
        :type user: pycalendar.tests.User
        :type global_event: pycalendar.tests.GlobalEvent
        :type title: basestring | None
        :type description: basestring | None
        :type event_type: pycalendar.events.models.EventType | None
        :type starting_date_time: datetime.datetime | None
        :type ending_date_time: datetime.datetime | None
        :type day: datetime.date | None
        :type rsvp: pycalendar.events.models.Rsvp | None
        """
        self.user = user
        self.global_event = global_event

        self.title = title
        self.description = description
        self.event_type = event_type
        self.starting_date_time = starting_date_time
        self.ending_date_time = ending_date_time
        self.day = day
        self.rsvp = rsvp

        self.pk = None


class LocalEventAdapter(RestApiAdapter):
        def __init__(self, client, local_event):
            """
            :type client: rest_framework.test.APIClient
            :type local_event: pycalendar.tests.events.LocalEvent
            """
            self.client = client
            self.local_event = local_event

        def create(self):
            """
            :rtype: dict
            """
            data = {
                'user': self.local_event.user.pk,
                'global_event': self.local_event.global_event.pk
            }
            response = self.client.post(
                path=reverse('local-event-list'),
                data=data,
                format='json'
            )

            if response.status_code != status.HTTP_201_CREATED:
                raise Exception(response.data)

            self.local_event.pk = response.data['id']

            return response.data

        def read(self):
            """
            :rtype: dict
            """
            response = self.client.get(
                path=reverse(
                    'local-event-detail',
                    kwargs={
                        'pk': self.local_event.pk,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.data)

            return response.data

        @staticmethod
        def read_all(client):
            """
            :type client: rest_framework.test.APIClient
            :rtype: dict
            """
            response = client.get(
                path=reverse('local-event-list'),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.data)

            return response.data

        @staticmethod
        def read_all_by_year(client, date_, zone):
            """
            :type client: rest_framework.test.APIClient
            :type date_: datetime.date
            :type zone: basestring
            :rtype: dict
            """
            response = client.get(
                path=reverse(
                    'local-event-list-by-y',
                    kwargs={
                        'year': date_.strftime('%Y'),
                        'zone': zone,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.data)

            return response.data

        @staticmethod
        def read_all_by_month(client, date_, zone):
            """
            :type client: rest_framework.test.APIClient
            :type date_: datetime.date
            :type zone: basestring
            :rtype: dict
            """
            response = client.get(
                path=reverse(
                    'local-event-list-by-ym',
                    kwargs={
                        'year': date_.strftime('%Y'),
                        'month': date_.strftime('%m'),
                        'zone': zone
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.data)

            return response.data

        @staticmethod
        def read_all_by_date(client, date_, zone):
            """
            :type client: rest_framework.test.APIClient
            :type date_: datetime.date
            :type zone: basestring
            :rtype: dict
            """
            response = client.get(
                path=reverse(
                    'local-event-list-by-ymd',
                    kwargs={
                        'year': date_.strftime('%Y'),
                        'month': date_.strftime('%m'),
                        'day': date_.strftime('%d'),
                        'zone': zone,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.data)

            return response.data

        def update(self, title=None, description=None, event_type=None,
                   starting_date_time=None, ending_date_time=None, day=None,
                   rsvp=None):
            """
            :type title: basestring | None
            :type description: basestring | None
            :type event_type: pycalendar.events.models.EventType | None
            :type starting_date_time: datetime.datetime | basestring | None
            :type ending_date_time: datetime.datetime | basestring | None
            :type day: datetime.date | None
            :type rsvp: pycalendar.events.models.Rsvp | None

            :rtype: dict
            """
            data = self._get_update_payload(
                title=title,
                description=description,
                event_type=event_type,
                starting_date_time=starting_date_time,
                ending_date_time=ending_date_time,
                day=day,
                rsvp=rsvp,
            )

            response = self.client.put(
                path=reverse(
                    'local-event-detail',
                    kwargs={
                        'pk': self.local_event.pk,
                    },
                ),
                data=data,
                format='json')

            if response.status_code != status.HTTP_200_OK:
                raise Exception(response.data)

            self.local_event.title = response.data['title']
            self.local_event.description = response.data['description']
            self.local_event.event_type = response.data['event_type']
            self.local_event.starting_date_time = response.data[
                'starting_date_time'
            ]
            self.local_event.ending_date_time = response.data[
                'ending_date_time'
            ]
            self.local_event.day = response.data['day']
            self.local_event.rsvp = response.data['rsvp']

            return response.data

        def delete(self):
            """
            :rtype: dict
            """
            response = self.client.delete(
                path=reverse(
                    'local-event-detail',
                    kwargs={
                        'pk': self.local_event.pk,
                    },
                ),
                format='json'
            )

            if response.status_code != status.HTTP_204_NO_CONTENT:
                raise Exception(response.data)

            self.local_event = None

        def _get_update_payload(self, title=None, description=None,
                                event_type=None, starting_date_time=None,
                                ending_date_time=None, day=None, rsvp=None):
            """
            :type title: basestring | None
            :type description: basestring | None
            :type event_type: pycalendar.events.models.EventType | None
            :type starting_date_time: datetime.datetime | None
            :type ending_date_time: datetime.datetime | None
            :type day: datetime.date | None
            :type rsvp: pycalandar.events.models.Rsvp | None
            """
            if title is None:
                title = self.local_event.title

            if description is None:
                description = self.local_event.description

            if event_type is None:
                event_type = self.local_event.event_type

            if rsvp is None:
                rsvp = self.local_event.rsvp

            data = {
                'id': self.local_event.pk,
                'user' : self.local_event.user.pk,
                'title': title,
                'description': description,
                'event_type': int(event_type),
                'rsvp': int(rsvp),
            }

            if event_type == EventType.ALL_DAY:
                day = self.local_event.day if day is None else day

                data['day'] = day
                data['starting_date_time'] = None
                data['ending_date_time'] = None
            elif event_type == EventType.PERIOD:
                if starting_date_time is None:
                    starting_date_time = self.local_event.starting_date_time

                if ending_date_time is None:
                    ending_date_time = self.local_event.ending_date_time

                data['day'] = None
                data['starting_date_time'] = starting_date_time
                data['ending_date_time'] = ending_date_time
            else:
                raise Exception('Unsupported EventType')

            return data
