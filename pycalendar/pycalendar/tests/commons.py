# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


class RestApiAdapter(object):
    def create(self, *args, **kwargs):
        raise NotImplemented

    def read(self, *args, **kwargs):
        raise NotImplemented

    def update(self, *args, **kwargs):
        raise NotImplemented

    def delete(self, *args, **kwargs):
        raise NotImplemented

    @staticmethod
    def read_all(client):
        raise NotImplemented
