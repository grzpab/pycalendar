# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.urls import reverse
from rest_framework import status

from .commons import RestApiAdapter


class Calendar(object):
    def __init__(self, name, color=None):
        """
        :type name: basestring
        :type color: basestring | None
        """
        self.name = name
        self.color = color
        self.user_pk = None
        self.pk = None


class CalendarAccess(object):
    def __init__(self, user, calendar, access_level):
        """
        :type user: pycalendar.tests.User
        :type calendar: pycalendar.tests.Calendar
        :type access_level: pycalendar.calendars.models.CalendarAccessLevel
        """
        self.user = user
        self.calendar = calendar
        self.access_level = access_level
        self.pk = None


class CalendarAdapter(RestApiAdapter):
    def __init__(self, client, calendar):
        """
        :type client: rest_framework.test.APIClient
        :type calendar: Calendar
        """
        self.client = client
        self.calendar = calendar

    def create(self):
        """
        :rtype: dict
        """

        data = {
            'name': self.calendar.name,
        }

        if self.calendar.color is not None:
            data['color'] = self.calendar.color

        response = self.client.post(
            path=reverse('calendars-list'),
            data=data,
            format='json'
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.data)

        self.calendar.pk = response.data['id']
        self.calendar.user_pk = response.data['user']

        return response.data

    def update(self, name=None, color=None):
        """
        :type name: basestring | None
        :type color: basestring | None
        :rtype: dict
        """

        data = {
            'id': self.calendar.pk,
            'user': self.calendar.user_pk,
            'name': self.calendar.name if name is None else name,
            'color': self.calendar.color if color is None else color,
        }

        response = self.client.put(
            path=reverse('calendars-detail', kwargs={'pk': self.calendar.pk}),
            data=data,
            format='json',
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        self.calendar.name = response.data['name']
        self.calendar.color = response.data['color']

        return response.data

    @staticmethod
    def read_all(client):
        """
        :type client: rest_framework.test.APIClient
        :rtype: dict
        """
        response = client.get(reverse('calendars-list'), format='json')

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    def read(self):
        """
        :rtype: dict
        """

        response = self.client.get(
            path=reverse('calendars-detail', kwargs={'pk': self.calendar.pk}),
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        return response.data

    def delete(self):
        """
        :rtype: dict
        """

        response = self.client.delete(
            path=reverse('calendars-detail', kwargs={'pk': self.calendar.pk}),
            format='json'
        )

        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise Exception(response.data)

        self.calendar = None

        return response.data


class CalendarAccessAdapter(RestApiAdapter):
    def __init__(self, client, calendar_access):
        """
        :type client: rest_framework.test.APIClient
        :type calendar_access: CalendarAccess
        """
        self.client = client
        self.calendar_access = calendar_access

    def create(self):
        """
        :rtype: dict
        """
        data = {
            'user': self.calendar_access.user.pk,
            'calendar': self.calendar_access.calendar.pk,
            'access_level': int(self.calendar_access.access_level),
        }

        response = self.client.post(
            path=reverse('calendar-accesses-list'),
            data=data,
            format='json'
        )

        if response.status_code != status.HTTP_201_CREATED:
            raise Exception(response.data)

        self.calendar_access.pk = response.data['id']

        return response.data

    def update(self, access_level):
        """
        :type access_level: pycalendar.calendars.models.CalendarAccessLevel
        :rtype: dict
        """
        data = {
            'id': self.calendar_access.pk,
            'user': self.calendar_access.user.pk,
            'calendar': self.calendar_access.calendar.pk,
            'access_level': int(access_level)
        }

        response = self.client.put(
            path=reverse('calendar-accesses-detail', kwargs={
                'pk': self.calendar_access.pk
            }),
            data=data,
            format='json'
        )

        if response.status_code != status.HTTP_200_OK:
            raise Exception(response.data)

        self.calendar_access.access_level = access_level

        return response.data

    def delete(self):
        """
        :rtype: dict
        """
        response = self.client.delete(
            path=reverse('calendar-accesses-detail', kwargs={
                'pk': self.calendar_access.pk
            }),
            format='json'
        )

        if response.status_code != status.HTTP_204_NO_CONTENT:
            raise Exception(response.data)

        self.calendar_access = None

        return response.data
