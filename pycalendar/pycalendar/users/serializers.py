# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User
from django.utils.translation import ugettext
from rest_framework import serializers


class UserCreationSerializer(serializers.ModelSerializer):
    password_confirmation = serializers.CharField(required=True)

    class Meta(object):
        model = User
        fields = (
            'username',
            'email',
            'password',
            'password_confirmation',
        )

    def validate(self, data):
        if data['password'] != data['password_confirmation']:
            raise serializers.ValidationError(ugettext(
                'The password is different than it\'s confirmation'
            ))

        return data

    def create(self, validated_data):
        return User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password']
        )

    def to_representation(self, instance):
        return {
            'id': instance.id,
        }


class UserListSerializer(serializers.ModelSerializer):

    class Meta(object):
        model = User
        fields = (
            'pk',
            'username',
            'email'
        )
