# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User

from rest_framework import permissions

from rest_framework.generics import CreateAPIView
from rest_framework.generics import ListAPIView

from .serializers import UserCreationSerializer
from .serializers import UserListSerializer


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreationSerializer
    permission_classes = (permissions.AllowAny,)


class UserListViewSet(ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserListSerializer
    permission_classes = (permissions.IsAuthenticated,)
