# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ..tests import User
from ..tests import UserManager


class UserTest(APITestCase):
    def setUp(self):
        self.user = User.create()
        self.user_manager = UserManager(
            client=self.client,
            user=self.user,
        )

        self.user_manager.create()

    def test_unauthorized_access_to_user_list(self):
        response = self.client.get(reverse('users'), format='json')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_authorized_access_to_user_list(self):
        with self.user_manager.do_as_user():
            response = self.client.get(reverse('users'), format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            self.assertEqual(len(response.data), 1)
            self.assertDictEqual(dict(response.data[0]),
                {
                    'pk': 1,
                    'username': self.user.username,
                    'email': self.user.email,
                }
            )
