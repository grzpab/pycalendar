# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from colorful.fields import RGBColorField
from enumfields import EnumField
from enumfields import IntEnum

from django.contrib.auth.models import User
from django.db import models


class CalendarAccessLevel(IntEnum):
    READ_EVENTS = 1
    WRITE_ON_EVENTS = 2

    class Labels(object):
        READ_EVENTS = 'Permission to read the calendar content'
        WRITE_ON_EVENTS = 'Permission to modify the calendar content'


class Calendar(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    name = models.CharField(null=False, blank=False, max_length=255)
    color = RGBColorField(null=True)

    class Meta(object):
        unique_together = (
            ('user', 'name',),
        )


class CalendarAccess(models.Model):
    calendar = models.ForeignKey(Calendar, on_delete=models.CASCADE, null=False,
                                 related_name='accesses')
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    access_level = EnumField(CalendarAccessLevel, null=False)

    class Meta(object):
        unique_together = (
            ('calendar', 'user',),
        )
