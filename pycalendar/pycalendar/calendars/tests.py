# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework.test import APITestCase

from pycalendar.calendars.models import CalendarAccessLevel

from pycalendar.tests import Calendar
from pycalendar.tests import CalendarAdapter
from pycalendar.tests import CalendarAccess
from pycalendar.tests import CalendarAccessAdapter
from pycalendar.tests import UserManager
from pycalendar.tests import User


class TestCalendars(APITestCase):
    def setUp(self):
        super(TestCalendars, self).setUp()

        self.user1 = User.create()
        self.user2 = User.create()

        self.user1_manager = UserManager(client=self.client, user=self.user1)
        self.user2_manager = UserManager(client=self.client, user=self.user2)

    def test_crud_operations(self):
        with self.user1_manager.do_as_user():
            calendar = Calendar(name='My first calendar')
            calendar_adapter = CalendarAdapter(
                client=self.client,
                calendar=calendar,
            )

            # create
            creation_response_data = calendar_adapter.create()
            self.assertEqual(creation_response_data, {
                'id': creation_response_data['id'],
                'user': self.user1.pk,
                'name': 'My first calendar',
                'color': None,
            })

            # read (many)
            reading_response_data = CalendarAdapter.read_all(client=self.client)
            self.assertEqual(len(reading_response_data), 1)
            self.assertDictEqual(dict(reading_response_data[0]),
                                 creation_response_data)

            # update and read (single)
            update_response_data = calendar_adapter.update(
                name=None,
                color='FF00FF'
            )
            reading_response_data = calendar_adapter.read()
            self.assertDictEqual(update_response_data,
                                 reading_response_data)

            # delete
            calendar_adapter.delete()

    def test_access_for_different_users(self):
        calendar1 = Calendar(name='The first calendar of User1')
        calendar1_adapter = CalendarAdapter(
            client=self.client,
            calendar=calendar1
        )

        calendar2 = Calendar(name='The first calendar of User2')
        calendar2_adapter = CalendarAdapter(
            client=self.client,
            calendar=calendar2
        )

        with self.user1_manager.do_as_user():
            calendar1_creation_response_data = calendar1_adapter.create()

        with self.user2_manager.do_as_user():
            calendar2_creation_response_data = calendar2_adapter.create()

        def do_as_user(calendar_manager, calendar_creation_response_data,
                       other_calendar_manager):
            reading_response_data = CalendarAdapter.read_all(client=self.client)
            self.assertEqual(len(reading_response_data), 1)
            self.assertDictEqual(
                dict(reading_response_data[0]),
                calendar_creation_response_data
            )

            reading_response_data = calendar_manager.read()
            self.assertDictEqual(
                reading_response_data,
                calendar_creation_response_data
            )

            calendar_manager.update(
                name='New name for the calendar',
                color='FFFF00'
            )

            with self.assertRaises(Exception):
                other_calendar_manager.read()

            with self.assertRaises(Exception):
                other_calendar_manager.update()

            with self.assertRaises(Exception):
                other_calendar_manager.delete()

        with self.user1_manager.do_as_user():
            do_as_user(calendar1_adapter, calendar1_creation_response_data,
                       calendar2_adapter)

        with self.user2_manager.do_as_user():
            do_as_user(calendar2_adapter, calendar2_creation_response_data,
                       calendar1_adapter)

    def test_access_levels_for_different_users(self):
        calendar1 = Calendar(name='The first calendar of User1')
        calendar1_adapter = CalendarAdapter(
            client=self.client,
            calendar=calendar1
        )

        calendar2 = Calendar(name='The first calendar of User2')
        calendar2_adapter = CalendarAdapter(
            client=self.client,
            calendar=calendar2
        )

        with self.user1_manager.do_as_user():
            calendar1_adapter.create()

        with self.user2_manager.do_as_user():
            calendar2_adapter.create()

        with self.user1_manager.do_as_user():
            calendar_access = CalendarAccess(
                user=self.user2,
                calendar=calendar1,
                access_level=CalendarAccessLevel.READ_EVENTS
            )
            manager = CalendarAccessAdapter(
                client=self.client,
                calendar_access=calendar_access
            )
            manager.create()
            manager.update(access_level=CalendarAccessLevel.WRITE_ON_EVENTS)

        with self.user2_manager.do_as_user():
            reading_response_data = CalendarAdapter.read_all(client=self.client)
            self.assertEqual(len(reading_response_data), 2)

        with self.user1_manager.do_as_user():
            manager.delete()

        with self.user2_manager.do_as_user():
            reading_response_data = CalendarAdapter.read_all(client=self.client)
            self.assertEqual(len(reading_response_data), 1)
