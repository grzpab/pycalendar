# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response

from .models import Calendar
from .models import CalendarAccess
from .serializers import CalendarSerializer
from .serializers import CalendarAccessSerializer
from .services import CalendarService
from .services import CalendarAccessService


class CalendarView(viewsets.ModelViewSet):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def filter_queryset(self, queryset):
        user = self.request.user
        queryset = super(CalendarView, self).filter_queryset(queryset)
        return queryset.filter(CalendarService.filter_criteria(user))

    def create(self, request, *args, **kwargs):
        CalendarService.inject_user_from_request(
            user=request.user,
            data=request.data,
        )

        return super(CalendarView, self).create(
            request=request,
            args=args,
            kwargs=kwargs,
        )


class CalendarAccessView(viewsets.ModelViewSet):
    queryset = CalendarAccess.objects.all()
    serializer_class = CalendarAccessSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def filter_queryset(self, queryset):
        user = self.request.user
        queryset = super(CalendarAccessView, self).filter_queryset(queryset)
        return queryset.filter(CalendarAccessService.filter_criteria(user))

    def create(self, request, *args, **kwargs):
        if not self._check_access_for_create_and_update_operations(request):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return super(CalendarAccessView, self).create(
            request=request,
            args=args,
            kwargs=kwargs,
        )

    def update(self, request, *args, **kwargs):
        if not self._check_access_for_create_and_update_operations(request):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        return super(CalendarAccessView, self).update(
            request=request,
            args=args,
            kwargs=kwargs,
        )

    def destroy(self, request, *args, **kwargs):
        user_id = request.user.id
        instance = self.get_object()

        if not CalendarAccessService.check_access_for_d_method(
                user_id=user_id,
                calendar_access=instance,
            ):
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def _check_access_for_create_and_update_operations(self, request):
        return CalendarAccessService.check_access_for_cu_methods(
            user_id=request.user.id,
            calendar_id=request.data['calendar'],
        )
