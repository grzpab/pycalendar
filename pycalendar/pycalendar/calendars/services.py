# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.db.models import Q

from .models import Calendar


class CalendarService(object):

    @classmethod
    def filter_criteria(cls, user):
        return (
            Q(user=user) |
            Q(accesses__user=user)
        )

    @classmethod
    def inject_user_from_request(cls, user, data):
        data['user'] = user.id


class CalendarAccessService(object):

    @classmethod
    def filter_criteria(cls, user):
        return (
            Q(user=user) |
            Q(calendar__user=user)
        )

    @classmethod
    def check_access_for_cu_methods(cls, user_id, calendar_id):
        return Calendar.objects.filter(
            id=calendar_id,
            user__id=user_id,
        ).exists()

    @classmethod
    def check_access_for_d_method(cls, user_id, calendar_access):
        return user_id == calendar_access.calendar.user.id
