# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from drf_enum_field.serializers import EnumFieldSerializerMixin
from rest_framework import serializers

from .models import Calendar
from .models import CalendarAccess


class CalendarSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Calendar


class CalendarAccessSerializer(EnumFieldSerializerMixin, serializers.ModelSerializer):
    class Meta(object):
        model = CalendarAccess
